# NeuralNetwork GUI  
Shows forward propagation of neural network.   
## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
### Prerequisites  
```
PyQt5
```
### Installation
The required package can be installed with the command
```
pip install pyqt5
```
### Usage
Run the program with e.g.
```
python main.py
```
