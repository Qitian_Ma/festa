# -*- coding: utf-8 -*-
"""
Created on Sat Jul 27 14:49:57 2019

@author: Qitian
"""

from PyQt5.QtWidgets import QApplication, QPushButton, QLineEdit, QDialog, QGroupBox, QHBoxLayout, QVBoxLayout, QGridLayout, QLabel, QDesktopWidget
import sys
from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import QRect

class Window(QDialog):
    def __init__(self):
        super().__init__()
        
        self.title = "PyQt5 Window"
        self.fontsize = 25
        self.width, self.height = QDesktopWidget().screenGeometry().width(), QDesktopWidget().screenGeometry().height()
        self.compute()
        self.initWindow()
        self.showFullScreen()
#.
        
    
    def initWindow(self):
        self.setWindowTitle(self.title)
#        self.setFixedSize(self.width, self.height)
        
        pixmap = QtGui.QPixmap("background.png").scaled(self.width, self.height, QtCore.Qt.KeepAspectRatio)
        pixmap_logo = QtGui.QPixmap("logo.png").scaled(self.width / 5, self.height / 5, QtCore.Qt.KeepAspectRatio)
        self.backgroundWidth = pixmap.width()
        self.backgroundHeight = pixmap.height()
    
        self.background = QLabel(self)
#        self.background.setSize(QtCore.QSize(40, 40))
        self.background.setPixmap(pixmap)
        
        self.logo = QLabel(self)
        self.logo.setGeometry(QRect(self.backgroundWidth / 1.35, self.backgroundHeight / 20, self.width / 5, self.height / 5))
        self.logo.setPixmap(pixmap_logo)
        self.UILabels()
        self.UIButtons()
        self.UILineEdit()
        self.show()
    
    def compute(self):
        import random
        
        value_min = 0
        value_max = 7
        self.value11 = random.randint(value_min, value_max)
        self.value21 = random.randint(value_min, value_max)
        self.wt11_12 = random.randint(value_min, value_max)
        self.wt11_22 = random.randint(value_min, value_max)
        self.wt21_12 = random.randint(value_min, value_max)
        self.wt21_22 = random.randint(value_min, value_max)
        self.wt12_3 = random.randint(value_min, value_max)
        self.wt22_3 = random.randint(value_min, value_max)
        
        self.value12 = self.value11 * self.wt11_12 + self.value21 * self.wt21_12
        self.value22 = self.value11 * self.wt11_22 + self.value21 * self.wt21_22
        self.value3 = self.value12 * self.wt12_3 + self.value22 * self.wt22_3
        
    def UILabels(self):
        self.neuron11 = QLabel(str(self.value11), self)
        self.neuron11.move(self.backgroundWidth / 7.5, self.backgroundHeight / 6)
        self.neuron11.setFont(QtGui.QFont("Sanserif", 50, QtGui.QFont.Bold))
        self.neuron11.setStyleSheet("color:white")
        
        self.neuron21 = QLabel(str(self.value21), self)
        self.neuron21.move(self.backgroundWidth / 7.5, self.backgroundHeight / 1.5)
        self.neuron21.setFont(QtGui.QFont("Sanserif", 50, QtGui.QFont.Bold))
        self.neuron21.setStyleSheet("color:white")
        
        self.neuron12 = QLabel(str(self.value12), self)
        self.neuron12.move(self.backgroundWidth / 1.82, self.backgroundHeight / 6)
        self.neuron12.setFont(QtGui.QFont("Sanserif", 50, QtGui.QFont.Bold))
        self.neuron12.setStyleSheet("color:white")
        
        self.neuron3 = QLabel(str("???"), self)
        self.neuron3.move(self.backgroundWidth / 1.2, self.backgroundHeight / 2.5)
        self.neuron3.setFont(QtGui.QFont("Sanserif", 50, QtGui.QFont.Bold))
        self.neuron3.setStyleSheet("color:white")
        
        self.line11_12 = QLabel(str(self.wt11_12), self)
        self.line11_12.move(self.backgroundWidth / 2.87, self.backgroundHeight / 8.2)
        self.line11_12.setFont(QtGui.QFont("Sanserif", 50, QtGui.QFont.Bold))
        self.line11_12.setStyleSheet("color:black")
        
        print("11_12", self.wt11_12)
        
        self.line21_22 = QLabel(str(self.wt21_22), self)
        self.line21_22.move(self.backgroundWidth / 2.87, self.backgroundHeight / 1.43)
        self.line21_22.setFont(QtGui.QFont("Sanserif", 50, QtGui.QFont.Bold))
        self.line21_22.setStyleSheet("color:black")
        print("21_22", self.wt21_22)
        
        self.line21_12 = QLabel(str(self.wt21_12), self)
        self.line21_12.move(self.backgroundWidth / 2.5, self.backgroundHeight / 4)
        self.line21_12.setFont(QtGui.QFont("Sanserif", 50, QtGui.QFont.Bold))
        self.line21_12.setStyleSheet("color:black")
        print("21_12", self.wt21_12)
        
        self.line11_22 = QLabel(str(self.wt11_22), self)
        self.line11_22.move(self.backgroundWidth / 2.5, self.backgroundHeight / 1.75)
        self.line11_22.setFont(QtGui.QFont("Sanserif", 50, QtGui.QFont.Bold))
        self.line11_22.setStyleSheet("color:black")
        print("11_22", self.wt11_22)
        
        self.line12_3 = QLabel(str(self.wt12_3), self)
        self.line12_3.move(self.backgroundWidth / 1.4, self.backgroundHeight / 4.5)
        self.line12_3.setFont(QtGui.QFont("Sanserif", 50, QtGui.QFont.Bold))
        self.line12_3.setStyleSheet("color:black")
        print("12_3", self.wt12_3)
        
        self.line22_3 = QLabel(str(self.wt22_3), self)
        self.line22_3.move(self.backgroundWidth / 1.4, self.backgroundHeight / 1.72)
        self.line22_3.setFont(QtGui.QFont("Sanserif", 50, QtGui.QFont.Bold))
        self.line22_3.setStyleSheet("color:black")
        print("22_3", self.wt22_3)
        
        print("22", self.value22)
    
    def UILineEdit(self):
        self.lineedit = QLineEdit(self)
        self.lineedit.setFont(QtGui.QFont("Sanserif", 50, QtGui.QFont.Bold))
        self.lineedit.setGeometry(QRect(self.backgroundWidth / 1.82, self.backgroundHeight / 1.52, 200, 200))
        self.lineedit.textChanged.connect(self.onChange)
        
    def refresh(self):
        self.compute()
        
        self.neuron11.setText(str(self.value11))
        self.neuron21.setText(str(self.value21))
        self.neuron12.setText(str(self.value12))
#        
        self.line11_12.setText(str(self.wt11_12))
        self.line11_22.setText(str(self.wt11_22))
        self.line21_12.setText(str(self.wt21_12))
        self.line21_22.setText(str(self.wt21_22))
        self.line12_3.setText(str(self.wt12_3))
        self.line22_3.setText(str(self.wt22_3))
        
        self.button.setText("resetto")
        self.neuron3.setText("???")
        
    def UIButtons(self):
        self.button = QPushButton("resetto", self)
        self.button.setGeometry(QRect(self.backgroundWidth / 1.27, self.backgroundHeight / 1.4, 500, 200))
        self.button.setFont(QtGui.QFont("Sanserif", 30))
        self.button.setStyleSheet("color:white; background-color:darkblue")
        self.button.clicked.connect(self.onClick)
        
    def onClick(self):
        self.refresh()
        
    def onChange(self):
        try:
            answer = int(self.lineedit.text().strip())
            if answer == self.value22:
                print("notice", self.value3)
                self.neuron3.setText(str(self.value3))
                self.button.setText("continuo")
        except:
            pass
        return
    
if __name__ == "__main__":
    App = QApplication(sys.argv)
    window = Window()
    sys.exit(App.exec())